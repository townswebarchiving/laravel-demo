<?php

Route::get('/', function () {
    return redirect()->route('site.home');
});

Auth::routes();

Route::get('home', 'HomeController@index')->name('site.home');
Route::get('threads', 'ThreadsController@index');
Route::get('threads/create', 'ThreadsController@create');
Route::get('threads/{channel}/{thread}', 'ThreadsController@show');
Route::post('threads', 'ThreadsController@store')->middleware('csrf_check');
Route::get('threads/{channel}', 'ThreadsController@index');
Route::post('threads/{channel}/{thread}/replies', 'RepliesController@store')->middleware('csrf_check');
