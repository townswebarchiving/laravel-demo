<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\BelongsToUser;

class Reply extends Model
{
    use BelongsToUser;

    protected $guarded = [];
}
