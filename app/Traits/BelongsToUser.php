<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use App\User;

trait BelongsToUser {

    public static function bootBelongsToUser() {
        static::creating(function (Model $thread) {
            $thread->user_id = auth()->id();
        });
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
